var express = require('express');
var dataController = require('../controllers/getDataController');
var router = express.Router();

router.get('/data', dataController.dataRequest)

module.exports = router;